# @rlmcneary2/weko-render

## Usage

This package supports rendering for web components with the following features:

- stores state information for the web component
- a render cycle is started when state is changed

### WekoRenderMixin

`WekoRenderMixin` is a mixin that extends a class implementing a custom component. The class must
have a constructor and the constructor must call `super`.

👉 _The weko-render package includes TypeScript type support._

```ts
import { WekoRenderMixin } from "https://esm.sh/@rlmcneary2/weko-render";

class MyWebComponent extends WekoRenderMixin(HTMLElement) {
  constructor() {
    super();
  }

  // class implementation
}
```

### Weko attributes

none

#### Static HTML attributes

none

#### Runtime attributes

none

### API

TODO: methods and properties

#### Properties

## Installation

### As part of an ES module

Use "esm.sh" and import this module into a source file where you need to use
render.

```ts
import from "https://esm.sh/@rlmcneary2/weko-render";
```

### As part of a bundle

If you are bundling your source code you may need to use a dynamic `import` to
load the library like so:

```ts
async function main() {
  await import("https://esm.sh/@rlmcneary2/weko-render");
}
```

### Using a script element

The script can also be loaded in an HTML file, typically as part of the `<head>`
element.

```html
<head>
  <script src="https://esm.sh/@rlmcneary2/weko-render" type="module"></script>
</head>
```

### TypeScript support

To use Typescript during development you need to do three things.

1. Install the package locally from npm under devDependencies: `npm i -D
@rlmcneary2/weko-render`.
2. Update the tsconfig.json file's `compilerOptions.paths` with the local
   installation of the package.

```json
"paths": {
  "@rlmcneary2/weko-render": ["./node_modules/@rlmcneary2/weko-render/src/index"]
}
```

3. Create a `.d.ts` file somewhere in a path processed by tsconfig.json that
   declares a module for the remote address.

```ts
// remote.d.ts

// Allow all module names starting with "https://". This will suppress TS errors.
declare module "https://*";

// Map the URL to all the exports from the locally installed package.
declare module "https://esm.sh/@rlmcneary2/weko-render" {
  export * from "@rlmcneary2/weko-render";
}
```
