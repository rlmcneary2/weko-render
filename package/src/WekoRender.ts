import type { Action, Constructor, QueueAction } from "./types.ts";
import { WEKO_ATTRIBUTE_TEMPLATE_ID, isSetterFunction } from "./shared.ts";

export function WekoRenderMixin<T extends Constructor, S = unknown>(baseType: T) {
  /**
   * WARNING!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
   *
   * There is some weird TypeScript stuff going on in this mixin when types are generated (in other
   * words bugs in `tsc`) that were only resolved by arranging the code so that the class is defined
   * and then returned on a separate line. DO NOT try to make this code better by returning the
   * class directly!
   */

  //@ts-ignore
  class WekoRender<S> extends baseType {
    #animationFrameQueue: QueueAction[] = [];
    #animationFrameRequestId = 0;
    #doRender = false;
    #invokeRenderTimeout = 0;
    #pendingRenderActions: Map<string, Action> = new Map();
    #state: S | undefined;
    #templateAttached = false;

    constructor(...args: any[]) {
      super(...args);
    }

    /*
     * web component methods
     */
    // #region

    static get observedAttributes(): string[] {
      // @ts-ignore
      return [...(super.observedAttributes ?? [])];
    }

    connectedCallback(): void {
      // @ts-ignore
      super.connectedCallback && super.connectedCallback();

      setTimeout(() => {
        this.#doRender = true;
        this.#render();
      }, 0);
    }

    // #endregion

    /**
     * API
     */
    // #region

    /**
     * If a `WEKO_ATTRIBUTE_TEMPLATE_ID` attribute is provided attach the template. This code will
     * automatically attach an "open" shadowRoot if the `shadowRoot` property is null.
     * @param modifyClone If provided this will be invoked allowing the caller to change the
     * template clone. Might be useful if Weko attributes need to be set on the clone's child
     * elements.
     */
    attachTemplate(modifyClone?: (clone: DocumentFragment) => void) {
      if (this.#templateAttached) {
        return;
      }

      this.#templateAttached = true;

      let append: (() => void) | undefined;
      let template: DocumentFragment | undefined;
      const templateId = this.getAttribute(WEKO_ATTRIBUTE_TEMPLATE_ID);
      if (templateId) {
        template = this.#cloneTemplate(templateId) || undefined;
        if (template) {
          modifyClone && modifyClone(template);

          append = () => {
            if (template) {
              !this.shadowRoot && this.attachShadow({ mode: "open" });
              this.shadowRoot?.appendChild(template);
            }
          };
        }
      }

      append && this.setRenderAction("Base_append_template", append);
    }

    /**
     * Invoked based on state changes, before updating the DOM. Rendering involves two stages:
     * 1. In the body of `beforeRender` create elements or get values that will be set on elements,
     *    use one or more `setRenderAction` calls to provide callback functions that will be invoked
     *    to change the DOM.
     * 2. Inside those callback functions actually make changes to the DOM. These callback functions
     *    will be invoked in an animation frame.
     * @param state The current state.
     */
    beforeRender(state: S | undefined): void {
      // Create elements and things...
      //
      // use `setRenderAction` to include changes to be made to the DOM.
    }

    /**
     * Determine if state has changed, by default it does a simple comparison of the state object.
     * Override if you want to do a different kind of comparison.
     * @param current
     * @param next
     * @returns True if the current and next state are **not** the same.
     */
    changedState(current: S | undefined, next: S | undefined): boolean {
      return current !== next;
    }

    /**
     * Call within the `beforeRender` method to set an action that will be invoked to update the
     * DOM.
     * @param id The identifier for the action, if an identifier already exists the existing action
     * will be replaced with the new action.
     * @param action This callback is where DOM manipulation is done. You should create elements and
     * prep outside this callback, limit changes in the callback to things that actually change the
     * rendered DOM.
     */
    setRenderAction(id: string, action: Action): void {
      if (this.#pendingRenderActions.has(id)) {
        console.warn(
          `[${this.localName}]setAction: id '${id}' already exists; replacing existing action.`
        );
      }

      this.#pendingRenderActions.delete(id);
      this.#pendingRenderActions.set(id, action);
    }

    /**
     * Set a value in render state. This triggers a render cycle.
     * @param state Either a state value or update function. The update function will get the
     * current state as its argument.
     */
    setRenderState(state: S | ((current: S | undefined) => S)): void {
      this.#doRender = true;

      let nextState: S | undefined;
      if (isSetterFunction<S>(state)) {
        nextState = state(this.#state);
      } else {
        nextState = state;
      }

      if (!this.changedState(this.#state, nextState)) {
        return;
      }

      this.#state = nextState;

      if (this.#invokeRenderTimeout) {
        return;
      }

      const instance = this;
      this.#invokeRenderTimeout = window.setTimeout(() => {
        this.#invokeRenderTimeout = 0;
        instance.#render.call(instance);
      }, 0);
    }

    // #endregion

    #cloneTemplate(templateId: string): DocumentFragment | void {
      const template = document.querySelector(`#${templateId}`) as HTMLTemplateElement;

      if (!template) {
        throw Error(`There is no template in the body that matches the query '#${templateId}'.`);
      }

      const clone = template.content.cloneNode(true) as DocumentFragment;

      return clone;
    }

    async #requestAnimationFrame(options: QueueAction): Promise<void> {
      this.#animationFrameQueue.push(options);

      if (this.#animationFrameRequestId) {
        window.cancelAnimationFrame(this.#animationFrameRequestId);
      }

      this.#animationFrameRequestId = window.requestAnimationFrame(() => {
        this.#animationFrameRequestId = 0;

        this.#animationFrameQueue.forEach(({ action }) => action());

        this.#animationFrameQueue.length = 0;
      });
    }

    #render() {
      if (!this.#doRender) {
        return;
      }

      this.#doRender = false;

      this.beforeRender(this.#state);

      if (this.#pendingRenderActions.size) {
        this.#requestAnimationFrame({
          action: () => {
            this.#pendingRenderActions.forEach(action => action());
            this.#pendingRenderActions.clear();
          }
        });
      }
    }
  }

  return WekoRender<S>;
}
