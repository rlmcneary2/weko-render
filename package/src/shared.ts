export const WEKO_ATTRIBUTE_TEMPLATE_ID = "weko-template-id";

export function isSetterFunction<S>(obj: any): obj is (state: S) => S {
  return obj && typeof obj === "function";
}
