export interface Action {
  (): void;
}

export type Constructor<T = HTMLElement> = new (
  // eslint-disable-next-line @typescript-eslint/no-explicit-any
  ...args: any[]
) => T;

export interface QueueAction {
  action: () => void;
}

export interface QueueForFrame {
  (options: QueueOptions): Promise<void>;
}

export interface QueueOptions {
  action: () => void;
}
