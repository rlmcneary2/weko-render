import { describe, expect, it, vi } from "vitest";
import "@testing-library/jest-dom/vitest";
import type { WekoRenderType } from "./types";
import { WekoRenderMixin } from "./WekoRender";

type WekoRenderElement<S> = WekoRenderType<S> & HTMLElement;

describe("WekoRender.connectedCallback", () => {
  it("renders when connected", async () => {
    const elem = document.createElement("test-component") as TestComponent;
    const beforeRender = vi.spyOn(elem, "beforeRender");

    expect(beforeRender).toHaveBeenCalledTimes(0);

    document.body.append(elem);
    await vi.waitFor(() => expect(beforeRender).toHaveBeenCalledOnce());
    expect(beforeRender).toHaveBeenCalledOnce();
    expect(beforeRender).toHaveBeenLastCalledWith(undefined);
  });
});

describe("WekoRender.setRenderAction", () => {
  it("set state using an object", async () => {
    const elem = document.createElement("test-component") as TestComponent;
    const changedState = vi.spyOn(elem, "changedState");
    changedState.mockImplementation(() => true);
    const beforeRender = vi.spyOn(elem, "beforeRender");

    elem.setRenderState({ name: "NAME" });
    expect(changedState).toHaveBeenCalledOnce();
    expect(changedState).toHaveBeenLastCalledWith(undefined, { name: "NAME" });

    await vi.waitFor(() => expect(beforeRender).toHaveBeenCalledTimes(1));
    expect(beforeRender).toHaveBeenLastCalledWith({ name: "NAME" });
  });

  it("set state using a callback function", async () => {
    const elem = document.createElement("test-component") as WekoRenderElement<
      Partial<{ email: string; name: string }>
    >;
    const changedState = vi.spyOn(elem, "changedState");
    changedState.mockImplementation(() => true);
    const beforeRender = vi.spyOn(elem, "beforeRender");

    elem.setRenderState({ name: "NAME" });
    expect(changedState).toHaveBeenCalledTimes(1);
    expect(changedState).toHaveBeenLastCalledWith(undefined, { name: "NAME" });

    elem.setRenderState(current => ({ ...current, email: "EMAIL" }));
    expect(beforeRender).toHaveBeenCalledTimes(0);

    await vi.waitFor(() => expect(beforeRender).toHaveBeenCalledTimes(1));
    expect(beforeRender).toHaveBeenLastCalledWith({ email: "EMAIL", name: "NAME" });
  });
});

describe("WekoRender.setRenderAction", () => {
  it("single render action is invoked", async () => {
    const elem = document.createElement("test-component") as WekoRenderElement<
      Partial<{ captain: string; shieldStrength: number }>
    >;
    const beforeRender = vi.spyOn(elem, "beforeRender");
    const action = vi.fn();
    beforeRender.mockImplementation(() => {
      elem.setRenderAction("INIT", action);
    });

    elem.setRenderState({ captain: "CAPTAIN" });

    await vi.waitFor(() => expect(beforeRender).toHaveBeenCalledOnce());
    await vi.waitFor(() => expect(action).toHaveBeenCalledOnce());
    expect(action).toHaveBeenLastCalledWith();
  });

  it("replace render action", async () => {
    const elem = document.createElement("test-component") as WekoRenderElement<
      Partial<{ captain: string; shieldStrength: number }>
    >;
    const beforeRender = vi.spyOn(elem, "beforeRender");
    const initialAction = vi.fn();
    const replacementAction = vi.fn();
    beforeRender.mockImplementation(() => {
      elem.setRenderAction("INIT", initialAction);
      elem.setRenderAction("INIT", replacementAction);
    });

    elem.setRenderState({ captain: "CAPTAIN" });

    await vi.waitFor(() => expect(beforeRender).toHaveBeenCalledOnce());
    await vi.waitFor(() => expect(replacementAction).toHaveBeenCalledOnce());
    expect(initialAction).toHaveBeenCalledTimes(0);
    expect(replacementAction).toHaveBeenLastCalledWith();
  });

  it("multiple render actions are invoked", async () => {
    const elem = document.createElement("test-component") as WekoRenderElement<
      Partial<{ captain: string; shieldStrength: number }>
    >;
    const beforeRender = vi.spyOn(elem, "beforeRender");
    const initAction = vi.fn();
    const loadAction = vi.fn();
    beforeRender.mockImplementation(() => {
      elem.setRenderAction("INIT", initAction);
      elem.setRenderAction("LOAD", loadAction);
    });

    elem.setRenderState({ shieldStrength: 100 });

    await vi.waitFor(() => expect(beforeRender).toHaveBeenCalledOnce());
    await vi.waitFor(() => expect(initAction).toHaveBeenCalledOnce());
    expect(initAction).toHaveBeenLastCalledWith();
    expect(loadAction).toHaveBeenCalledOnce();
    expect(loadAction).toHaveBeenLastCalledWith();
  });

  it("clears render actions", async () => {
    const elem = document.createElement("test-component") as WekoRenderElement<
      Partial<{ captain: string; shieldStrength: number }>
    >;
    const beforeRender = vi.spyOn(elem, "beforeRender");
    const action = vi.fn();
    beforeRender.mockImplementationOnce(() => {
      elem.setRenderAction("INIT", action);
    });

    elem.setRenderState({ shieldStrength: 99 });

    await vi.waitFor(() => expect(beforeRender).toHaveBeenCalledOnce());
    await vi.waitFor(() => expect(action).toHaveBeenCalledOnce());
    expect(beforeRender).toHaveBeenLastCalledWith({ shieldStrength: 99 });
    expect(action).toHaveBeenLastCalledWith();

    elem.setRenderState({ shieldStrength: 98 });

    await vi.waitFor(() => expect(beforeRender).toHaveBeenCalledTimes(2));
    expect(beforeRender).toHaveBeenLastCalledWith({ shieldStrength: 98 });
    expect(action).toHaveBeenCalledTimes(1);
  });
});

class TestComponent extends WekoRenderMixin(HTMLElement) {
  constructor() {
    super();
  }
}

customElements.define("test-component", TestComponent);
