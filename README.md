# @rlmcneary2/weko-render

## Usage

See the [package README](./package/README.md).

## Development

Let's change the code in this repo!

### Install

```sh
npm install
```

### Build

Build the source files (output to "dist" directory).

```sh
npm run build
```

### Scripts

- `build` - generate all run-time and publish-time files.
- `build:copy` - copy files to "dist" required for publishing.
- `build:source` - transpile source TypeScript to JavaScript.
- `build:types` - generate ".d.ts" files in "dist" from TypeScript source.
- `clean` - delete everything in "dist."
- `postbuild` - delete temp files that `tsc` creates.
- `test` - run all the tests
